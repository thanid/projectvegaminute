<nav class="navbar navbar-expand-lg navbar-dark bg-custom">
  <a class="navbar-brand" href="#">VEG A MINUTE</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <div class="searchnav">
      <div class="active-cyan-4 ">
        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
      </div>
    </div>
    <ul class="navbar-nav ml-auto">
    <li class="nav-item active">
          <a class="nav-link" href="#"><img src="imge/home.png" class="icon-nav"><span class="sr-only">(current)</span></a>
        </li>
        <div class="linenav border-right mt-3"></div>
        <li class="nav-item">
          <a class="nav-link" href="#"><img src="imge/profile.png" class="icon-nav"></a>
        </li>
        <div class="linenav border-right mt-3"></div>
        <li class="nav-item">
          <a class="nav-link" href="#"><img src="imge/fw.png" class="icon-nav"></a>
        </li>
        <div class="linenav border-right mt-3"></div>
        <li class="nav-item">
          <a class="nav-link" href="#"><img src="imge/chatt.png" class="icon-nav"></a>
        </li>
        <div class="linenav border-right mt-3"></div>
        <li class="nav-item">
          <a class="nav-link" href="#"><img src="imge/bell.png" class="icon-nav"></a>
        </li>
        <div class="linenav border-right mt-3"></div>
      <a class="nav-item nav-link disabled" href="#"><img src="imge/shopping-cart.png" class="icon-nav"></a>
      <a class="nav-item nav-link disabled" href="#"><img src="imge/help.png" class="icon-nav"></a>
        <li class="nav-item dropdown">
          <a class="nav-item nav-link dropdown-toggle mr-md-2" href="#" id="bd-versions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="bd-versions">
            <a class="dropdown-item" href="#">ตั้งค่า</a>
            <a class="dropdown-item" href="#">บันทึกกิจกรรม</a>
            <a class="dropdown-item" href="#">Log out</a>
          </div>
        </li>
    </ul>
  </div>
</nav>