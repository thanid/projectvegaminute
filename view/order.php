  <!--<div class="row shop-tracking-status">
    <div class="col-md-12">
        <div class="well">
            <div class="order-status">
                <div class="order-status-timeline">
                    <div class="order-status-timeline-completion c3"></div>
                </div>
                <div class="image-order-status image-order-status-new active img-circle">
                    <span class="status">Accepted</span>
                    <div class="icon"></div>
                </div>
                <div class="image-order-status image-order-status-intransit active img-circle">
                    <span class="status">Shipped</span>
                    <div class="icon"></div>
                </div>
                <div class="image-order-status image-order-status-completed active img-circle">
                    <span class="status">Completed</span>
                    <div class="icon"></div>
                </div>
            </div>
        </div>
    </div>
</div>-->

<div class="row shop-tracking-status">
    
    <div class="col-md-12">
        <div class="well">
    
            <h4>Your order status:</h4>

            <ul class="list-group">
                <li class="list-group-item">
                    <span class="prefix">Date created:</span>
                    <span class="label label-success">12.12.2013</span>
                </li>
                <li class="list-group-item">
                    <span class="prefix">Last update:</span>
                    <span class="label label-success">12.15.2013</span>
                </li>
                <li class="list-group-item">
                    <span class="prefix">Tracking Number:</span>
                    <span class="label label-success">12345689</span>
                </li>
               <!-- <li class="list-group-item">
                    <span class="prefix">Comment:</span>
                    <span class="label label-success">customer's comment goes here</span>
                </li>
                <li class="list-group-item">You can find out latest status of your order with the following link:</li>
                <li class="list-group-item"><a href="//tracking/link/goes/here">//tracking/link/goes/here</a></li>-->
            </ul>

            <div class="order-status">

                <div class="order-status-timeline">
                    <!-- class names: c0 c1 c2 c3 and c4 -->
                    <div class="order-status-timeline-completion c3"></div>
                </div>

                <div class="image-order-status image-order-status-new active img-circle">
                    <span class="status">Accepted</span>
                    <div class="icon"></div>
                </div>
                <div class="image-order-status image-order-status-intransit active img-circle">
                    <span class="status">Shipped</span>
                    <div class="icon"></div>
                </div>
                <div class="image-order-status image-order-status-completed active img-circle">
                    <span class="status">Completed</span>
                    <div class="icon"></div>
                </div>

            </div>
        </div>
    </div>

</div>